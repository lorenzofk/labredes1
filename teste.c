/*-------------------------------------------------------------*/
/* Nomes                     */
/*-------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>  //estrutura ifr
#include <net/ethernet.h>
#include <netinet/ether.h>
#include <netinet/in_systm.h> //tipos de dados
#include <netinet/in.h> //definicao de protocolos
#include <unistd.h>
#include <arpa/inet.h> //funcoes para manipulacao de enderecos IP
#include <errno.h>
#include <string.h>
#include <linux/if_ether.h>
//#include <pthread.h>
#include <sys/time.h>

#define ETHERTYPE_LEN 2
#define MAC_ADDR_LEN 6
#define IPV4_LEN 20
#define ICMP_LEN 16
#define BUFFER_LEN 1528
#define BUFFSIZE 1518
#define ETHERTYPE 0x800
#define PROTOCOL 01

typedef unsigned char MacAddress[MAC_ADDR_LEN];

extern int errno;

struct timeval stop, start;
double t1,t2;
int received, lost;
int count = 1;
				
char **arguments = NULL;

//pthread_t threads[2];

unsigned short calcsum(unsigned short *buffer, int length) {
	unsigned long sum; 	

	// initialize sum to zero and loop until length (in words) is 0 
	for (sum=0; length>1; length-=2) // sizeof() returns number of bytes, we're interested in number of words 
		sum += *buffer++;	// add 1 word of buffer to sum and proceed to the next 

	// we may have an extra byte 
	if (length==1)
		sum += (char)*buffer;

	sum = (sum >> 16) + (sum & 0xFFFF);  // add high 16 to low 16 
	sum += (sum >> 16);		     // add carry 
	return ~sum;
}

void copy_string(char d[], char s[]){
	int c = 0;
	while(s[c] != '\0'){
		d[c] = s[c];
		c++;	
	}
	d[c] = '\0';
} 

void envia(void *arg){
	struct sockaddr_ll destAddr;
	int sockFd = 0, retValue = 0;
	int ipOrigemForSend[4], ipDestinoForSend[4];
	long macLocalForSend[6], macDestForSend[6];
	
	//Ethertype - IP: 0x0800
	short int etherTypeT = htons(0x0800);
	
	//Pacote a ser enviado
	char buffer[BUFFER_LEN];
	
	//O argumento recebido via console é um ponteiro pra ponteiro, por isso o "**"
	char argumento1[15];
	char argumento2[17];
	char argumento3[15];
	char argumento4[17];
	arguments = (char **)arg;
	copy_string(argumento1,arguments[1]);
	//argumento1[14] = '\0';
	copy_string(argumento2,arguments[2]);
	//argumento2[16] = '\0';
	copy_string(argumento3,arguments[3]);
	//argumento3[14] = '\0';
	copy_string(argumento4,arguments[4]);
	//argumento4[16] = '\0';

	char *ipOrigemToken = NULL;
    char *ipDestinoToken = NULL;
	char *macLocalToken = NULL;
	char *macDestinoToken = NULL;
	
	unsigned short checksumIPv4, checksumICMP;
	
	ipOrigemToken = strtok(argumento1, ".");
	
	int i = 0;
	
	while(ipOrigemToken != NULL) {
		ipOrigemForSend[i] = atoi(ipOrigemToken);

		ipOrigemToken = strtok(NULL, ".");
		i++;
	}
	
	i = 0;
	
	ipDestinoToken = strtok(argumento3, ".");
	
	while(ipDestinoToken != NULL) {
		ipDestinoForSend[i] = atoi(ipDestinoToken);
		ipDestinoToken = strtok(NULL, ".");
		i++;
	}

	macLocalToken = strtok(argumento2, ":");
	
	i = 0;
	
	while(macLocalToken != NULL) {
		macLocalForSend[i] = strtol(macLocalToken, NULL, 16);
		macLocalToken = strtok(NULL, ":");
		i++;
	}

	macDestinoToken = strtok(argumento4, ":");
	
	i = 0;
	
	while(macDestinoToken != NULL) {
		macDestForSend[i] = strtol(macDestinoToken, NULL, 16);
		macDestinoToken = strtok(NULL, ":");
		i++;
	}
	
	if((sockFd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
		printf("Erro na criacao do socket.\n");
		exit(1);
	}

	//Identicacao de qual maquina (MAC) deve receber a mensagem enviada no socket.
	destAddr.sll_family = htons(PF_PACKET);
	destAddr.sll_protocol = htons(ETH_P_ALL);
	destAddr.sll_halen = 6;
	destAddr.sll_ifindex = 3;  //Índice da interface pela qual os pacotes serao enviados. 2 = eth0, 3 = wlan0
	//memcpy(&(destAddr.sll_addr), destMac, MAC_ADDR_LEN);

	// Escreve o cabecalho Ethernet no buffer
	
	//Preenche o cabeçalho Ethernet com o Mac Destino	
	memcpy(buffer, &macDestForSend[0], 1);
	memcpy(buffer+1, &macDestForSend[1], 2);
	memcpy(buffer+2, &macDestForSend[2], 3);
	memcpy(buffer+3, &macDestForSend[3], 4);
	memcpy(buffer+4, &macDestForSend[4], 5);
	memcpy(buffer+5, &macDestForSend[5], 6);


	//Preenche o cabeçalho Ethernet com o Mac Origem
	memcpy((buffer+MAC_ADDR_LEN), &macLocalForSend[0], 1);
	memcpy((buffer+MAC_ADDR_LEN)+1, &macLocalForSend[1], 1);
	memcpy((buffer+MAC_ADDR_LEN)+2, &macLocalForSend[2], 1);
	memcpy((buffer+MAC_ADDR_LEN)+3, &macLocalForSend[3], 1);
	memcpy((buffer+MAC_ADDR_LEN)+4, &macLocalForSend[4], 1);
	memcpy((buffer+MAC_ADDR_LEN)+5, &macLocalForSend[5], 1);


	memcpy((buffer+(2*MAC_ADDR_LEN)), &(etherTypeT), sizeof(etherTypeT));

	//Monta o cabeçalho IPv4 sem os ip's(origem e destino)
	char paramsIPv4[] = {0x45, 0x00, 0x00, 0x32, 0xa3, 0x43, 0x40, 0x00, 0x40, 0x01, 0x00, 0x00};
	
	//Preenche o cabeçalho com o ip origem
	memcpy(&paramsIPv4[12], &ipOrigemForSend[0], 1);
	memcpy(&paramsIPv4[13], &ipOrigemForSend[1], 1);
	memcpy(&paramsIPv4[14], &ipOrigemForSend[2], 1);
	memcpy(&paramsIPv4[15], &ipOrigemForSend[3], 1);
	
	//Preenche o cabeçalho com o ip destino
	memcpy(&paramsIPv4[16], &ipDestinoForSend[0], 1);
	memcpy(&paramsIPv4[17], &ipDestinoForSend[1], 1);
	memcpy(&paramsIPv4[18], &ipDestinoForSend[2], 1);	
	memcpy(&paramsIPv4[19], &ipDestinoForSend[3], 1);
	
	//Escreve o cabeçalho IPv4 no buffer
	memcpy((buffer+ETHERTYPE_LEN+(2*MAC_ADDR_LEN)), paramsIPv4, IPV4_LEN);

	//Variável que armazena o valor de checksum do IPv4
	checksumIPv4 = calcsum((unsigned short int*) paramsIPv4, 20);
	
	//Preenche o checksum do cabeçalho IPv4
	memcpy((buffer+ETHERTYPE_LEN+(2*MAC_ADDR_LEN))+10, &checksumIPv4, 2);

	//printf("Cksum IPv4: %x\n", htons(checksumIPv4));

	//Cabecalho ICMP
	char paramsICMP[] = {0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	
	//Escreve o cabeçalho ICMP no buffer(ainda sem o checksum calculado)
	memcpy((buffer+ETHERTYPE_LEN+(2*MAC_ADDR_LEN))+IPV4_LEN, paramsICMP, 30);

	//Variável que armazena o valor de checksum do ICMP
	checksumICMP = calcsum((unsigned short *) paramsICMP, 30);
	
	//Preenche o checksum do cabeçalho ICMP
	memcpy((buffer+ETHERTYPE_LEN+(2*MAC_ADDR_LEN))+IPV4_LEN+2, &checksumICMP, 2);

	//printf("Cksum ICMP: %x\n", htons(checksumICMP));

	//Envia pacote(s) de 64 bytes
	if((retValue = sendto(sockFd, buffer, 64, 0, (struct sockaddr *)&(destAddr), sizeof(struct sockaddr_ll))) < 0) {
		printf("ERROR! sendto() \n");
		exit(1);
	}
	/* Inicia timer do RTT */
	/* Timer comecando do envio do pacote*/
	gettimeofday(&start, NULL);
	t1 += start.tv_sec+(start.tv_usec)/1000;
}

void recebe() {
	struct ifreq ifr;	
	int sockd;
	int on;
	unsigned char *data;
	unsigned char buff1[BUFFSIZE]; // buffer de recepcao
	
	if((sockd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
		printf("Erro na criacao do socket.\n");
		exit(1);
	}

	// O procedimento abaixo eh utilizado para "setar" a interface em modo promiscuo
	strcpy(ifr.ifr_name, "wlan0");
	if(ioctl(sockd, SIOCGIFINDEX, &ifr) < 0)
		printf("erro no ioctl!");
	
	ioctl(sockd, SIOCGIFFLAGS, &ifr);
	ifr.ifr_flags |= IFF_PROMISC;
	ioctl(sockd, SIOCSIFFLAGS, &ifr);

	struct timeval loopStop, loopStart;
	double loopTimeout;
	
	/* Comeco do tempo do Timeout */
	gettimeofday(&loopStart, NULL);
	// recepcao de pacotes
	while (1) {
		/* Testa o Timeout */
		gettimeofday(&loopStop, NULL);
		loopTimeout = (loopStop.tv_sec - loopStart.tv_sec)+((loopStop.tv_usec - loopStart.tv_usec)/1000000);

		if(loopTimeout > 5){
			printf("Timeout... \n");
			lost++;
			break;
		}		
		
		unsigned char mac_dst[6];
		unsigned char mac_src[6];
		unsigned char params[20];
		unsigned char jump[8];
		short int ethertype;
		
		/* Recebe pacotes */
		recv(sockd,(char *) &buff1, sizeof(buff1), MSG_DONTWAIT);
		
		/* Termina timer do RTT */
		/* Timer terminando quando chega o pacote*/
		gettimeofday(&stop, NULL);
		t2 += stop.tv_sec+(stop.tv_usec)/1000;
	
		/* Copia o conteudo do cabecalho Ethernet */
		memcpy(mac_dst, buff1, sizeof(mac_dst));
		memcpy(mac_src, buff1+sizeof(mac_dst), sizeof(mac_src));
		memcpy(&ethertype, buff1+sizeof(mac_dst)+sizeof(mac_src), sizeof(ethertype));
		memcpy(params, buff1 + sizeof(mac_dst) + sizeof(mac_src) + sizeof(ethertype) + sizeof(jump), sizeof(params));
		ethertype = ntohs(ethertype);
					
		if (ethertype == ETHERTYPE && params[1] == PROTOCOL) {
			
			if((int) params[12] == 8) {
				//printf("Echo Request enviado\n");
			}
			else if((int) params[12] == 0) {
				printf("\n################################\n");			

				printf("Pacote %d - Echo Reply recebido \n", count);
				
				count++;		
			
				printf("MAC origem:  %02x:%02x:%02x:%02x:%02x:%02x\n", mac_src[0], mac_src[1], mac_src[2], mac_src[3], mac_src[4], mac_src[5]);
				printf("MAC destino: %02x:%02x:%02x:%02x:%02x:%02x\n", mac_dst[0], mac_dst[1], mac_dst[2], mac_dst[3], mac_dst[4], mac_dst[5]);
				printf("TTL: %d\n", (int) params[0]);
				printf("RTT: %.2g ms\n",(t2-t1));
				printf("IP Origem: %d.%d.%d.%d\n", params[4], params[5], params[6], params[7]);
				printf("IP Destino: %d.%d.%d.%d\n", params[8], params[9], params[10], params[11]);
			
				printf("################################\n");
			
				printf("\n");
				received++;
				break;
			}
		}
	}
}

int main(int argc, char *argv[]) {
	if(argc == 5) {
		int i = 0;
		while (i < 6){
			/* Inicializa o timer */
			t1 = 0.0; t2 = 0.0;
			envia((void *) argv);
			recebe();
			sleep(1);
			i++;
		}

		printf(" --- ping statistics ---\n");
		printf("6 packets transmitted, %d received, %d lost \n", received, lost);
	}
	else {
		printf("\nErro ao iniciar programa. Você deve informar os parâmetros corretamente. Exemplo:\n");
		printf("./ping IP_Origem MAC_Origem IP_Destino MAC_Destino \n\n");
		exit(1);	
	}
}
